const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const purgecss = require('gulp-purgecss');


//compile scss into css
function style() {
    //1.where is my scss
    return gulp.src('src/scss/**/*.scss') //gets all files ending with .scss in src/scss
    .pipe(sourcemaps.init())
    //2. pass that file through sass compiler
    .pipe(sass({outputStyle: 'compressed'}).on('error',sass.logError))
    
    // .pipe(purgecss({
    //     css: ['src/css/external.css'],
    //     content: ['src/**/*.html']
    // }))
    
    // .pipe(sourcemaps.write())
    //3. where do I save the compiled css file
    .pipe(gulp.dest('src/css'))
    //4. stream change to all browsers
    .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./src",
            index: "/index.html"
        }
    });
    gulp.watch('src/scss/**/*.scss', style);
    gulp.watch('src/*.html').on('change',browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;